from qiskit import QuantumRegister, ClassicalRegister, QuantumCircuit, execute, Aer
from math import pi
import operator

# Controlled-NOT operation from all search qubits to ancilla qubit.
def giant_ccx(circuit, q_reg, q_count_for_search):
	k = q_count_for_search # Number of qubits that remain to be put as control-qubits.
	i = 0 # Index of next control-qubit.
	j = i+q_count_for_search #Index of next target-qubit.
	operations_done = [] # Used to store all qubit triplets, that were used in ccx operations.

	# Apply ccx to qubits until all needed qubits affect ancilla qubit.
	while k>1:
		k = k//2
		for s in range(k):
			circuit.ccx(q_reg[i], q_reg[i+1], q_reg[j])
			operations_done.append([i,i+1,j])
			i+=2 
			j+=1
		k=j-i # Update remaining number of qubits that will be used as control.
		
	# Reverse all ccx operations, except the last one (that was applied to ancilla qubit)
	k = len(operations_done)-1
	for s in range(k-1,-1,-1):
		circuit.ccx(q_reg[operations_done[s][0]], q_reg[operations_done[s][1]], q_reg[operations_done[s][2]])

# Diffusion transformation, uses Phase-kickback.
def diffusion(circuit, q_reg, q_count_for_search):
	q_count_total = 2*q_count_for_search - 1
	# Prepare ancilla qubit in state |->.
	circuit.x(q_reg[q_count_total-1])
	circuit.h(q_reg[q_count_total-1])
	
	for i in range(q_count_for_search):
		circuit.h(q_reg[i])
		circuit.x(q_reg[i])

	giant_ccx(circuit, q_reg, q_count_for_search)
	
	circuit.x(q_reg[q_count_total-1])
	
	for i in range(q_count_for_search):
		circuit.x(q_reg[i])
		circuit.h(q_reg[i])
	
	# Put ancilla qubit back into state |0>.
	circuit.h(q_reg[q_count_total-1])
	circuit.x(q_reg[q_count_total-1])
	
# Flips the sign of the amplitude of one marked element, uses Phase-kickback.
def oracle(circuit, q_reg, q_count_for_search, element_num):
	q_count_total = 2*q_count_for_search - 1
	# Prepare ancilla qubit in state |->.
	circuit.x(q_reg[q_count_total-1])
	circuit.h(q_reg[q_count_total-1])
	
	two_in_degree = 2
	for i in range(q_count_for_search):
		if(element_num%two_in_degree < two_in_degree/2):
			circuit.x(q_reg[i])
		two_in_degree*=2

	giant_ccx(circuit, q_reg, q_count_for_search)
	
	two_in_degree = 2
	for i in range(q_count_for_search):
		if(element_num%two_in_degree < two_in_degree/2):
			circuit.x(q_reg[i])
		two_in_degree*=2
	
	# Put ancilla qubit back into state |0>.
	circuit.h(q_reg[q_count_total-1])
	circuit.x(q_reg[q_count_total-1])
	
# Prepares Oracle transformation for all marked elements, elements are given as a list.
def oracle_all(circuit, q_reg, q_count_for_search, elements):
	# In this loop we do an Oracle transformation by marking elements one by one.
	for i in range(len(elements)):
		oracle(circuit, q_reg, q_count_for_search,elements[i])
	
# Outputs the optimal number of Grover's iterations to find the marked element.
def get_optimal_iteration_count(q_count_for_search, marked_element_count):
	total_element_count = 2**q_count_for_search
	return int((pi*(total_element_count**0.5)) // (4*(marked_element_count**0.5)))

# Grover's search, all steps except measurement.
def grover(circuit, q_reg, q_count_for_search, elements, iteration_count=-1):
	# Put each qubit of search space into superposition.
	for i in range(q_count_for_search):
		circuit.h(q_reg[i])
	if(iteration_count < 0):
		iteration_count = get_optimal_iteration_count(q_count_for_search, len(elements))
	# Each iteration involves Oracle call and diffusion.
	for i in range(iteration_count):
		oracle_all(circuit, q_reg, q_count_for_search, elements)
		diffusion(circuit, q_reg, q_count_for_search)

# Reflection operation on a qubit counterclockwise on 2D plane by theta
# Rotation(theta) = [[cos(theta), -sin(theta)],[sin(theta), cos(theta)]]
# Reflection(theta) = [[cos(2theta), sin(2theta)],[sin(2theta), -cos(2theta)]]
# Reflection(theta) = apply Z, then Rotation(2theta)
def reflection(circuit, qubit, theta):
	circuit.z(qubit)
	# 2*theta to make rotation by theta on 2D plane, we need to rotate by 2*theta.
	circuit.ry(2*2*theta, qubit)
	
# H = Reflection(pi/8) on 2D plane
# Diffusion: D = H R H^-1
# H = H^-1
# Relpace H with A - reflection by angle theta on 2D plane, or,
# Replace H with A - rotation by angle theta on 2D plane
def parametrized_diffusion(circuit, q_reg, q_count_for_search, theta, is_reflection):
	
	# A part of Diffusion:
	if(is_reflection):
		for i in range(q_count_for_search):
			reflection(circuit, q_reg[i], theta)
	else:
		for i in range(q_count_for_search):
			circuit.ry(2*theta, q_reg[i])
	
	# R part of Diffusion starts here
	q_count_total = 2*q_count_for_search - 1
	# Prepare ancilla qubit in state |->.
	circuit.x(q_reg[q_count_total-1])
	circuit.h(q_reg[q_count_total-1])
	
	for i in range(q_count_for_search):
		circuit.x(q_reg[i])

	giant_ccx(circuit, q_reg, q_count_for_search)
	
	circuit.x(q_reg[q_count_total-1])
	
	for i in range(q_count_for_search):
		circuit.x(q_reg[i])
	
	# Put ancilla qubit back into state |0>.
	circuit.h(q_reg[q_count_total-1])
	circuit.x(q_reg[q_count_total-1])
	# R part of Diffusion ends here
	
	# A^-1 part of Diffusion:
	if(is_reflection):
		for i in range(q_count_for_search):
			reflection(circuit, q_reg[i], theta)
	else:
		for i in range(q_count_for_search):
			circuit.ry(-2*theta, q_reg[i])

# Grover's search with parametrized diffusion, all steps except measurement.
def parametrized_grover(circuit, q_reg, q_count_for_search, elements, iteration_count, theta, is_reflection):
	# Put each qubit of search space into superposition.
	for i in range(q_count_for_search):
		circuit.h(q_reg[i])
	
	# Each iteration involves Oracle call and diffusion.
	for i in range(iteration_count):
		oracle_all(circuit, q_reg, q_count_for_search, elements)
		parametrized_diffusion(circuit, q_reg, q_count_for_search, theta, is_reflection)

# Grover's search for unknown number of elements.
# Iteratively tries by assuming that there is 1,2,4,...,2^(n-1) elements marked,
# if did not found, repets from assuming 1 marked element.
# To show more realistic behavior of the routine, shots=1.
def grover_unknown_count(q_count_for_search, elements, oracle_is_function=False):
	total_element_count = 2**q_count_for_search
	guess_marked_count = 1
	found = False
	while(found == False):
		# Grover's search circuit - generated, assumint, that number of marked elements is guess_marked_count.
		qreg =  QuantumRegister(q_count_for_search*2-1)
		creg = ClassicalRegister(q_count_for_search*2-1)
		mycircuit = QuantumCircuit(qreg,creg)
		for i in range(q_count_for_search):
			mycircuit.h(qreg[i])
		optimal_iterations = get_optimal_iteration_count(q_count_for_search, guess_marked_count)
		for i in range(optimal_iterations):
			if(oracle_is_function):
				oracle_from_function(mycircuit, qreg, q_count_for_search, elements)
			else:
				oracle_all(mycircuit, qreg, q_count_for_search, elements)
			diffusion(mycircuit, qreg, q_count_for_search)
		mycircuit.measure(qreg,creg)
		# Execute the circuit for one time (shots=1), to imitate real-life algorithm.
		job = execute(mycircuit,Aer.get_backend('qasm_simulator'),shots=1)
		counts = job.result().get_counts(mycircuit)
		found_element = int(list(counts.keys())[0],2)
		#found_element = int(max(counts.items(), key=operator.itemgetter(1))[0], 2)
		# Prints found element and checks, whether it was marked.
		print('Grover found element:',found_element, ', assumed number of marked elements:', guess_marked_count)
		if(oracle_is_function):
			if(elements(found_element) == 1):
				found = True
		else:
			if (found_element in elements):
				found = True
		if(found == False):
			guess_marked_count *= 2
			if(guess_marked_count == total_element_count):
				guess_marked_count = 1
				
# Prepares Oracle transformation for all marked elements, oracle is given as a function.
def oracle_from_function(circuit, q_reg, q_count_for_search, o_function):
	for i in range(2**q_count_for_search):
		if(o_function(i) == 1):
			oracle(circuit, q_reg, q_count_for_search, i)