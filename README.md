# Grover-step-by-step

In this library we implement Grover's search step by step with basic gates. It has several features for overall flexibility and parameterization of the operations. We use Qiskit and Python for the implementation.